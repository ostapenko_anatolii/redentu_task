import {connect} from 'react-redux';
import {Fingerprints} from './Fingerprints';
import {bindActionCreators} from 'redux';
import {setFingerPrints} from '../../action/index'


const  mapStateToProps = (state)=>{
    return {
        fingerPrints: [
            {
                id: 1,
                url: 'BahamaBlue.jpg',
                color: '#315edd'
            },
            {
                id: 2,
                url: 'BambooLeaves.jpg',
                color: '#96ac64'
            },
            {
                id: 3,
                url: 'CottageIvy.jpg',
                color: '#217541'
            },
            {
                id: 4,
                url: 'LilacPosies.jpg',
                color: '#b63292'
            },
            {
                id: 5,
                url: 'PearTart.jpg',
                color: '#ddda44'
            },
            {
                id: 6,
                url: 'PottersClay.jpg',
                color: '#d38740'
            },
            {
                id: 7,
                url: 'RhubarbStalk.jpg',
                color: '#b81d50'
            },
            {
                id: 8,
                url: 'Tangelo.jpg',
                color: '#ef8a4e'
            }
        ], finaleOrder: state.finaleOrder
    }
};
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({setFingerPrints},dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(Fingerprints);