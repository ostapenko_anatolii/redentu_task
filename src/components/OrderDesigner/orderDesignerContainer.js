import {connect} from 'react-redux';
import {OrderDesigner} from './OrderDesigner';

const  mapStateToProps = (state)=>{
    return {
        orderDesigner: state.orderDesigner
    }
}

export default connect(mapStateToProps)(OrderDesigner);