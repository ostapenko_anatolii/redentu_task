import React, { Component } from 'react';
import Tabs from'../Tabs/tabsContainer';
import SelectedResult from '../SelectedResult/selectedResultContainer';
import SelectPicture from '../SelectPicture/selectPictureContainer';
import SelectFrame from '../SelectFrame/selectFrameContainer';
import Title from '../Title/titleContainer';
import Fingerprints from '../Fingerprints/fingerprintsContainer';
import Sum from '../Sum/sumContainer';

export class OrderDesigner extends Component{
    getCurentView(){
        var step = this.props.orderDesigner.step;
            switch (step) {
                case 0:
                    return <SelectPicture/>
                break;
                case 1:
                    return  <SelectFrame/>
                break;
                case 2:
                    return  <Title/>
                case 3:
                    return  <Fingerprints/>
                break;
            }
    }
    render(){
        return(
            <div className='order-designer-wrapper row'>
                <div className='container col-md-8'>
                    <Tabs/>
                    {this.getCurentView()}
                    <Sum/>
                </div>
                <SelectedResult/>
            </div>
        )
    }
};
